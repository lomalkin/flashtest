#!/bin/bash
IF=$1
DIR=$2

CHKSIZE="1024"		#bytes
REPORT="quickreport.txt"

SIZE=$(ls -l | grep "$IF" | awk '{ print $5 }')
SIZEM=$(($SIZE/1024/1024))

QUICKHASH=$(cat $IF | head -c "$CHKSIZE" | md5sum | awk '{ print $1 }')
echo "Quick hash: $QUICKHASH"

rm "$REPORT"
for I in $(ls $DIR);
do
	QH=$(cat "$DIR"/"$I" | head -c "$CHKSIZE" | md5sum | awk '{ print $1 }')
	echo "$QH" "$I" | tee -a "$REPORT"
done

HASH_EMPTY="d41d8cd98f00b204e9800998ecf8427e"

CNT=$(ls -l "$DIR" | grep "$IF" | wc -l)
EMPTY=$(grep "$HASH_EMPTY" "$REPORT" | wc -l)
CORRECT=$(grep "$QUICKHASH" "$REPORT" | wc -l)

echo "Checking files count: $CNT"
echo "Correct files: $CORRECT / $CNT"
echo "Empty files: $EMPTY / $CNT"
SPACE=$(($SIZE*$CORRECT/1024/1024))
echo "Real available space estimate: "$SPACE"M"
echo
echo "Wrong bin files list:"
grep -v "$QUICKHASH" "$REPORT"
echo
notify-send "Target test finish."
