#!/bin/bash
IF=$1
DIR=$2
CHKFILE="checking.txt"

SIZE=$(ls -l | grep "$IF" | awk '{ print $5 }')
SIZEM=$(($SIZE/1024/1024))
HASH=$(cat md5sum.txt | grep $IF | awk '{ print $1 }')
echo "File: $IF"
echo "File size: $SIZE ("$SIZEM"M)"
echo "Correct hash: $HASH"


CNT=$(ls -l "$DIR" | grep "$IF" | wc -l)
echo "Checking files count: $CNT"
echo
echo "Checking hashes will start in 3 sec..."
sleep 3
md5sum "$DIR"/*"$IF" | tee $CHKFILE
echo "Checking hashes finished."
echo

HASH_EMPTY="d41d8cd98f00b204e9800998ecf8427e"

EMPTY=$(grep "$HASH_EMPTY" "$CHKFILE" | wc -l)
CORRECT=$(grep "$HASH" "$CHKFILE" | wc -l)

echo "Checking files count: $CNT"
echo "Correct files: $CORRECT / $CNT"
echo "Empty files: $EMPTY / $CNT"
SPACE=$(($SIZE*$CORRECT/1024/1024))
echo "Real available space estimate: "$SPACE"M"
echo
echo "Wrong bin files list:"
grep -v "$HASH" "$CHKFILE"
echo
notify-send "Target test finish."
