#!/bin/bash
IF=$1
DIR=$2

echo "Will start filling $DIR with file $IF in 3 sec..."
sleep 3

i=0
while :
do
	FNAME=$(printf "%06d" $i)"_$IF"
	cp -v "$IF" "$DIR"/"$FNAME"
	if [ ! "$?" -eq "0" ]; then
		echo "Error."
		sync
		echo "Please remount flash and run: ./checkflash.sh $IF $DIR."
		notify-send "Target is full"
		exit 1;
	fi
	i=$((i+1))
done
