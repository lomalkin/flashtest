# FlashTest
## Overview

This is simplest scripts, that can be used for checking flash drives and other storage media for real storage capacity.
It can be useful for check flash drives with possible fake visible size to avoid real user files corruption.

Usually some of bogus flash drives and cards have some small real size, like 64MB or 512MB, but formatted as 2GB, 8GB, 32GB, etc.

## Principles of work
Scripts working from "user" side, it don't care about formatting, fake firmware of target flash and so on.

It only can help you:

1. ./mkbin.sh - Make binary file with desired size in MB
2. ./fillflash.sh - Copy it to target flash many times, until target will be full.
3. Target need be reconnected/remounted manually (!) for some reasons.
4. ./checkflash.sh - Read files and check it's checksums. Just run it with the same args and see report.
5. ./rmbin.sh - Clean all the binaries in cd.
